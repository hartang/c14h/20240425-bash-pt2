#!/usr/bin/env bash
set -euo pipefail

# This works, but shows the output of grep
if grep "NAME" /etc/os-release; then
    echo "'NAME' detected"
fi

# This works, but doesn't show output
if grep "NAME" /etc/os-release &>/dev/null; then
    echo "'NAME' detected"
fi

# This works the other way round
if ! grep "NO_NAME" /etc/os-release &>/dev/null; then
    echo "'NO_NAME' missing"
fi
