#!/usr/bin/env bash
set -euo pipefail

# Create testfile
FILE="testme"
touch "$FILE"

# Make it executable if it isn't already
[[ -x "$FILE" ]] || chmod +x "$FILE"

# Remove the testfile if it exists
[[ -e "$FILE" ]] && rm "$FILE"
