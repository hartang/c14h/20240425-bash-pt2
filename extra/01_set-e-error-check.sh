#!/usr/bin/env bash
set -euo pipefail

set +e
false
declare -i EXIT_CODE=$?
set -e
echo "exit code is '$EXIT_CODE'"

declare -i EXIT_CODE
EXIT_CODE="$(false; echo $?)"
echo "exit code is '$EXIT_CODE'"

# This breaks
false
EXIT_CODE=$?
echo "exit code is '$EXIT_CODE'"
