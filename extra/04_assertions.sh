#!/usr/bin/env bash
set -euo pipefail

FILE="/tmp/missing.txt"

function _fatal {
    echo "FATAL: $*" 1>&2
    exit 1
}

[[ -f "$FILE" ]] ||
    _fatal "file '$FILE' not found"
