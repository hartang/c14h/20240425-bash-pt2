#!/usr/bin/env bash
set -ETeuo pipefail

# Log message at "error" severity
function _err  { echo -e "\e[31mERROR\e[0m  $*"; }
# Log message at "error" severity and terminate execution. For this to terminate execution, the
# calling shell must set the `-e` bash option (errexit). Adds information to track callers if setup
# correctly (See `_caller` below).
function _fatal {
    _err_ctx print
    _err "$* $(_caller)"
    exit 1
}

function _oh_nooo {
    _err_ctx push "failed to frobnicate the foobars"
    [[ "$1" -gt 2 ]] || _fatal "unexpected input '$1'"
    _err_ctx pop
}

# Storage for error context messages
declare -a __ERROR_CTX
# Decides if we track caller information
declare __ERROR_TRACK_CALLER=0

# Determine the caller of whoever called this function (2 stackframes up) and print it in the 
# format `(calling_file@lineno)`.
#
# **IMPORTANT**: This only works when `__ERROR_TRACK_CALLER` has value 0!
function _caller {
    if [[ "${__ERROR_TRACK_CALLER:-1}" -eq 0 ]]; then
        declare -a CALLER
        readarray -d' ' -t CALLER <<< "$(caller 1)"
        echo "(${CALLER[2]%$'\n'}@${CALLER[0]})"
    fi
}

# Manipulate the error context.
#
# Provides three subcommands:
#
# - push: Store the given message as error context for later retrieval
# - pop: Pop the most recently stored error context and remove it
# - print: Called by the default `SIGERR` trap defined below
function _err_ctx {
    declare CMD
    declare -i i=0
    [[ "$#" -ge 1 ]] || _fatal "'_err_ctx' needs at least one arg"
    CMD="$1"
    shift 1
    case "$CMD" in
        "push")
            __ERROR_CTX+=("$* $(_caller)")
            ;;
        "pop")
            NERR="${#__ERROR_CTX[@]}"
            if [[ "$NERR" -gt 0 ]]; then
                unset "__ERROR_CTX[$((NERR-1))]" || true
            fi
            ;;
        "print")
            # check if context empty
            [[ -n "${__ERROR_CTX[*]:-""}" ]] || return 0
            NERR="${#__ERROR_CTX[@]}"
            [[ "$NERR" -gt 0 ]] || return 0
            for ((i=0;i<${#__ERROR_CTX[@]};i++)); do
                _err "${__ERROR_CTX[$i]}"
            done
            ;;
        *)
            _err "unknown command '$CMD'"
            _fatal "did you mean/forget 'push' or 'pop'?"
            ;;
    esac
}
# Print all error contexts when an unhandled error occurs
trap "_err_ctx print" ERR


_err_ctx push "failed to parse arguments"
_oh_nooo 1
_err_ctx pop
