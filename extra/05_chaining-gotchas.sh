#!/usr/bin/env bash
set -euo pipefail

declare -i PORT=65534
[[ $PORT -ge 1 ]] &&
    [[ $PORT -le 65535 ]] ||
    _fatal "PORT must be an int between 1 and 65535"



FILE="testme.sh"
cat > "$FILE" <<EOF
#!/usr/bin/env bash
false
EOF

# expected, file isn't executable
[[ -x "$FILE" ]] &&
    bash "$FILE" ||
    echo "'$FILE' isn't executable"

# Unexpected: File is executable, but it returns a nonzero exit code, so we
# enter the `||` case/branch
chmod +x "$FILE"
[[ -x "$FILE" ]] &&
    bash "$FILE" ||
    echo "'$FILE' isn't executable"

# Turning the order around doesn't improve things: The `echo` call always
# returns true, so we will now run `$FILE` unconditionally!
[[ -x "$FILE" ]] ||
    echo "'$FILE' isn't executable" &&
    bash "$FILE" 
