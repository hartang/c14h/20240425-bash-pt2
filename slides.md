---
title: Bash und Ich (Teil 2)
author: Andreas Hartmann (@hartan)
patat:
  breadcrumbs: true
  wrap: true
  slideLevel: 2
  margins:
    left: 5
    right: 5
    top: 1
    bottom: 1
  eval:
    figlet:
      command: figlet -s -w 70 -f big
      fragment: false
      replace: true
...

```figlet
Bash und Ich (Teil 2)
```

# Error-Handling

- Vorheriger Vortrag:
    - <https://gitlab.com/hartang/c14h/20240418-bash-pt1>


## Motivation

- Fehler passieren - ständig
- Fehler blind weitergeben ist kaum hilfreich
    - Fehlertext selten ausreichend (*file not found*)
    - Kein Kontext (*Welche Datei?!*)
    - Keine Hilfe für Anwender (*Was nun?*)


# Fehler detektieren

## Komponenten und Werkzeuge

- Wichtige Komponente: `set -e`
    - Für wenn doch was komisches passiert...
- Exit-Codes prüfen: `$?`
    - Enthält exit-code des letzten Programmaufrufs
    - Nicht direkt kompatibel mit `set -e`
      ```bash
      set +e; false; declare -i EXIT_CODE=$?; set -e
      # ODER
      declare -i EXIT_CODE="$(false; echo "$?")"
      ```
- Befehl als Argument in `if` einbauen


## Exit-Codes in `if`

- *Tipp*: Output unterdrücken mit `&>/dev/null`
  ```bash
  if grep "keyword" file.txt &>/dev/null; then
      echo "'keyword' detected"
  fi
  ```

. . .

- Ergebnis negieren mit `!` (*Leerzeichen beachten*)
  ```bash
  if ! grep "keyword" file.txt &>/dev/null; then
      echo "'keyword' missing"
  fi
  ```


# Pipeline lists

## `&&` und `||`

- Ähnlich logischen Operatoren
- `&&`: Wenn exit-code *==* 0, führe nächsten Befehl aus
- `||`: Wenn exit-code *!=* 0, führe nächsten Befehl aus

. . .

- Operationen verketten:

```bash
[[ -e "$FILE" ]] && rm "$FILE"

[[ -x "$FILE" ]] || chmod +x "$FILE"
```


## Tests verketten

```bash
if [[ -f "$FILE_A" ]] && [[ -x "$FILE_B" ]]; then
    # ...
elif [[ -d "$DIR_A" ]] &&
    grep "bla" "$FILE_C" &>/dev/null; then
    # ...
else
    echo "something broke, duh" 1>&2
fi
```


## Assertions

```bash
function _fatal {
    echo "FATAL: $*" 1>&2
    exit 1
}

[[ -f "$FILE" ]] ||
    _fatal "file '$FILE' not found"
```

Ausgesprochen:  
> '$FILE' muss eine Datei sein, sonst ...


## Achtung

```bash
[[ $PORT -ge 1 ]] &&
    [[ $PORT -le 65535 ]] ||
    _fatal "PORT must be an int between 1 and 65535"
```
. . .

**ABER**
```bash
[[ -x "$FILE" ]] &&
    bash "$FILE" ||
    echo "'$FILE' isn't executable"
```
**Kein if-then-else!**


# Fehler für Fortgeschrittene

- Siehe `scripts/06_advanced-errors.sh`
- Siehe Mitschnitt (TODO)


# Fragen?

```figlet
EOF
```
