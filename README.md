# Bash und Ich - Error Handling

Nachdem wir letztes mal (c14h No. 614, 2024-04-18) gesehen haben, wie man
einige Fehler in Bash von vornherein vermeidet, geht es heute um dem Umgang mit
Fehlern abseits des sofortigen Programmabbruchs.

Ich möchte euch zeigen, welche Mittel bash zur Fehlerbehandlung zur Verfügung
stellt und welche Tücken es dabei unter Umständen gibt.

Hier befinden sich die "Folien" zu einem Vortrag, den ich am 25.04.2024 im
Rahmen der c14h beim NoName e.V. gehalten habe. Die Aufnahme zum Vortrag ist
hier verlinkt: https://www.noname-ev.de/chaotische_viertelstunde.html#c14h_615


## Ausführen

Die Präsentation wird in einem Container ausgeführt. Dazu benötigt man `podman`
oder `docker` auf dem Rechner. Der folgende Befehl lädt dann den Container
herunter oder baut ihn lokal und startet die Präsentation:

```
$ ./present.sh
```
